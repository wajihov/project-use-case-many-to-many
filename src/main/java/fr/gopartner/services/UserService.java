package fr.gopartner.services;

import fr.gopartner.entities.Role;
import fr.gopartner.entities.User;

public interface UserService {

	User addUser(User user);

	Role addNewRole(Role role);

	User findUserByName(String name);

	Role findRoleByNomRole(String role);

	void addRoleToUser(String username, String roleName);

	User authentification(String username, String passwrd);

}

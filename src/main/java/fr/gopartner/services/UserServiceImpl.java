package fr.gopartner.services;

import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import fr.gopartner.entities.Role;
import fr.gopartner.entities.User;
import fr.gopartner.repositories.RoleRepository;
import fr.gopartner.repositories.UserRepository;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	private RoleRepository roleRepository;

	public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
		super();
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
	}

	@Override
	public User addUser(User user) {
		user.setIdUser(UUID.randomUUID().toString());
		return userRepository.save(user);
	}

	@Override
	public Role addNewRole(Role role) {
		return roleRepository.save(role);
	}

	@Override
	public User findUserByName(String name) {
		return userRepository.findByName(name);
	}

	@Override
	public Role findRoleByNomRole(String role) {
		return roleRepository.findByNomRole(role);
	}

	@Override
	public void addRoleToUser(String username, String roleName) {
		User user = findUserByName(username);
		Role role = findRoleByNomRole(roleName);
		if (user.getRoles() != null) {
			user.getRoles().add(role);
			role.getUsers().add(user);
		}
		// userRepository.save(user);
	}

	@Override
	public User authentification(String username, String passwrd) {
		User u = userRepository.findByName(username);
		if (u != null) {
			if (u.getPassword().equals(passwrd))
				return u;
			else
				throw new RuntimeException("Password incorrect ");
		} else {
			throw new RuntimeException("Mauvaise authentification");
		}
	}

}

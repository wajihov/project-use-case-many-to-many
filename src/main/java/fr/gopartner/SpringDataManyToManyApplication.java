package fr.gopartner;

import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import fr.gopartner.entities.Role;
import fr.gopartner.entities.User;
import fr.gopartner.services.UserService;

@SpringBootApplication
public class SpringDataManyToManyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataManyToManyApplication.class, args);
	}

	@Bean
	CommandLineRunner start(UserService userService) {
		return args -> {
			System.out.println(" Hello World");
			User user = new User();
			user.setName("user1");
			user.setEmail("user1@jo.fr");
			user.setPassword("123456");
			userService.addUser(user);

			User user2 = new User();
			user2.setName("admin");
			user2.setEmail("admin@jo.com");
			user2.setPassword("456321");
			userService.addUser(user2);

			Stream.of("STUDENT", "USER", "ADMIN").forEach(r -> {
				Role r1 = new Role();
				r1.setNomRole(r);
				userService.addNewRole(r1);
			});

			userService.addRoleToUser("user1", "STUDENT");
			userService.addRoleToUser("user1", "USER");
			userService.addRoleToUser("admin", "USER");
			userService.addRoleToUser("admin", "ADMIN");

			try {
				User u = userService.authentification("user1", "123456");
				System.out.println(" l'utilsateur est : " + u.getIdUser() + " -> Name :  " + u.getName());
				u.getRoles().forEach(i -> {
					System.out.println("les role : " + i.getNomRole());
					// System.out.println("le role est : " + i.getNomRole());
				});
				
			} catch (Exception e) {
				e.printStackTrace();
			}

		};
	}

}

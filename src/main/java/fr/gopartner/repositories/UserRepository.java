package fr.gopartner.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.gopartner.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

	User findByName(String name);

}

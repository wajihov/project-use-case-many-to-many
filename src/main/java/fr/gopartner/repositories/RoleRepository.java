package fr.gopartner.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.gopartner.entities.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

	Role findByNomRole(String nomRole);

}

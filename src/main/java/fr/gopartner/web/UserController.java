package fr.gopartner.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import fr.gopartner.entities.User;
import fr.gopartner.repositories.UserRepository;
import fr.gopartner.services.UserServiceImpl;

@RestController
public class UserController {

	@Autowired
	private UserServiceImpl userServiceImpl;

	@Autowired
	private UserRepository userRepository;

	@GetMapping(path = "/users/{userName}")
	public User user(@PathVariable("userName") String name) {
		return userServiceImpl.findUserByName(name);
	}

	@GetMapping(path = "/users")
	public List<User> getUser() {
		return userRepository.findAll();
	}

}
